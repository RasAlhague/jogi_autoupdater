mod update;

use self::update::Updater;
use structopt::StructOpt;

type StartedCallback = fn();
type FinishedCallback = fn();
type TransferringCallback = fn(u32, u32);
type ErroredCallback = fn(&str, &str);
type StatusChangedCallback = fn(&str);
type UnzippedFileCallback = fn(u32, &str, u64);

pub enum Callback {
    Started(StartedCallback),
    Finished(FinishedCallback),
    StatusChanged(StatusChangedCallback),
    Errored(ErroredCallback),
    Transferring(TransferringCallback),
    UnzippedFile(UnzippedFileCallback),
}

#[derive(Clone)]
struct GithubLinkBuilder {
    username: String,
    repo: String,
    subpath: Option<String>,
}

impl GithubLinkBuilder {
    fn new(username: &str, repo: &str) -> GithubLinkBuilder {
        GithubLinkBuilder {
            username: String::from(username),
            repo: String::from(repo),
            subpath: None,
        }
    }

    fn with_subpath(&mut self, subpath: &str) -> Self {
        self.subpath = Some(String::from(subpath));
        self.clone()
    }

    fn unset_subpath(&mut self) -> Self {
        self.subpath = None;
        self.clone()
    }

    fn build(&self) -> String {
        if let Some(subpath) = &self.subpath {
            return format!(
                "https://github.com/{}/{}/{}",
                self.username, self.repo, subpath
            );
        }

        return format!("https://github.com/{}/{}/", self.username, self.repo);
    }
}

fn write_status_message(message: &str) {
    println!("{}", message);
}

fn write_started() {
    println!("Started update process ...");
}

fn write_finished() {
    println!("Finished update process ...");
}

fn write_error(error: &str, message: &str) {
    println!("An error has occured!");
    println!("Error: {}", error);
    println!("Message: {}", message);
}

fn write_transferring(current: u32, total: u32) {
    println!("Transferring file {} of {} ...", current, total);
}

fn write_unzipped_file(count: u32, path: &str, byte_size: u64) {
    println!("File {} extracted to \"{}\" ({} bytes)", count, path, byte_size);
}

#[derive(Debug, StructOpt)]
#[structopt(name = "jogi_autoupdater", about = "An tool to automaticly update the game scrax made. Will probably never be used XD.")]
struct Config {
    #[structopt(short = "g", long = "game-file-path")]
    game_file_path: Option<String>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let conf = Config::from_args();

    println!(
        "{} v.{}\n",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );

    let path = match &conf.game_file_path {
        Some(p) => std::path::Path::new(p),
        None => std::path::Path::new("./Jogigame"),
    };

    if !path.exists() {
        std::fs::create_dir_all(path)?;
    }

    let canonicalized_path = std::fs::canonicalize(path).unwrap();
    println!("Game file path: {}", canonicalized_path.display());

    let updater = Updater::new(path.to_str().unwrap())
        .set_callback(Callback::StatusChanged(write_status_message))
        .set_callback(Callback::Started(write_started))
        .set_callback(Callback::Finished(write_finished))
        .set_callback(Callback::Errored(write_error))
        .set_callback(Callback::UnzippedFile(write_unzipped_file))
        .set_callback(Callback::Transferring(write_transferring));

    updater.update("Scraxtastic", "JogisWayToStreamingGod")?;

    Ok(())
}

