use std::fs::DirEntry;
use crate::{
    Callback, ErroredCallback, FinishedCallback, GithubLinkBuilder, StartedCallback,
    StatusChangedCallback, TransferringCallback, UnzippedFileCallback,
};
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::path::PathBuf;

pub struct Updater {
    update_path: String,
    status_changed: Option<StatusChangedCallback>,
    started: Option<StartedCallback>,
    finished: Option<FinishedCallback>,
    errored: Option<ErroredCallback>,
    transferring: Option<TransferringCallback>,
    unzipped_file: Option<UnzippedFileCallback>,
}

impl Updater {
    pub fn new(update_path: &str) -> Updater {
        Updater {
            update_path: String::from(update_path),
            status_changed: None,
            started: None,
            finished: None,
            errored: None,
            transferring: None,
            unzipped_file: None,
        }
    }

    pub fn set_callback(mut self, callback: Callback) -> Self {
        match callback {
            Callback::Started(sc) => self.started = Some(sc),
            Callback::Finished(fc) => self.finished = Some(fc),
            Callback::Errored(ec) => self.errored = Some(ec),
            Callback::StatusChanged(scc) => self.status_changed = Some(scc),
            Callback::Transferring(tc) => self.transferring = Some(tc),
            Callback::UnzippedFile(ufc) => self.unzipped_file = Some(ufc),
        };

        self
    }

    pub fn update(
        self,
        github_user: &str,
        github_repo: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        self.started();

        let mut github_link_builder = GithubLinkBuilder::new(github_user, github_repo);

        self.status_changed("Fetching online version ...");
        let latest_version = self.get_latest_version(&mut github_link_builder)?;

        self.status_changed("Comparing versions ...");

        if let Ok(cv) = get_current_version() {
            if cv == latest_version {
                self.status_changed(&format!("The current version {} is up to date!", cv));
                self.finished();

                return Ok(());
            }

            self.status_changed(&format!(
                "Current version is {}, latest version is {}. Commencing with updating ...",
                cv, latest_version
            ));
        } else {
            self.status_changed("No game found yet, installing game!");
        }

        self.status_changed(&format!(
            "Start downloading for version {} game files ...",
            latest_version
        ));
        let zip_file_path = self.download(&mut github_link_builder)?;

        self.status_changed("Start unzipping files ...");
        let unzip_folder = self.unzip(&zip_file_path)?;
        self.status_changed("Finished unzipping files ...");

        self.status_changed("Start replacing files ...");
        self.transfer_files(unzip_folder)?;

        set_current_version(&latest_version)?;

        self.finished();

        Ok(())
    }

    fn get_latest_version(
        &self,
        github_link_builder: &mut GithubLinkBuilder,
    ) -> Result<String, Box<dyn std::error::Error>> {
        let link = github_link_builder.with_subpath("releases/latest").build();

        let resp = reqwest::blocking::get(link)?;

        let version_url = resp.url().as_str();
        let version_split: Vec<&str> = version_url.split("/").collect();

        let version = version_split[version_split.len() - 1];

        return Ok(String::from(version));
    }

    fn download(
        &self,
        github_link_builder: &mut GithubLinkBuilder,
    ) -> Result<String, Box<dyn std::error::Error>> {
        use std::fs;

        let link = github_link_builder
            .with_subpath("releases/latest/download/JogisWayToStreamingGod_Data.zip")
            .build();

        self.status_changed(&format!("Downloading zipfile from '{}' ... ", &link));
        let bytes = reqwest::blocking::get(link)?.bytes()?;

        self.status_changed("Building temp path ...");

        let temp_path = env::temp_dir();
        let base_path = Path::new(temp_path.to_str().unwrap());
        let zip_file_path = base_path
            .join("jogiupdater")
            .join("JogisWayToStreamingGod_Data.zip");

        let zip_file_str = zip_file_path.to_str().unwrap_or("Invalid zip file path!");

        self.status_changed(&format!("Saving file to '{}' ...", zip_file_str));

        if !temp_path.join("jogiupdater").exists() {
            fs::create_dir(format!("{}/jogiupdater", temp_path.to_str().unwrap()))?;
        }

        if zip_file_path.exists() {
            fs::remove_file(zip_file_path.clone())?;
        }

        let mut file = File::create(zip_file_path.clone())?;
        file.write_all(&bytes)?;

        Ok(String::from(zip_file_str))
    }

    fn errored(&self, error: &str, message: &str) {
        if let Some(e) = self.errored {
            e(error, message);
        }
    }

    fn started(&self) {
        if let Some(s) = self.started {
            s();
        }
    }

    fn finished(&self) {
        if let Some(f) = self.finished {
            f();
        }
    }

    fn transferring(&self, current: u32, total: u32) {
        if let Some(t) = self.transferring {
            t(current, total);
        }
    }

    fn status_changed(&self, message: &str) {
        if let Some(s) = self.status_changed {
            s(message);
        }
    }

    fn unzipped_file(&self, count: u32, path: &str, byte_size: u64) {
        if let Some(uf) = self.unzipped_file {
            uf(count, path, byte_size);
        }
    }

    fn unzip(&self, source: &str) -> Result<PathBuf, Box<dyn std::error::Error>> {
        use std::fs;
        use std::io;
    
        let file = fs::File::open(source)?;
    
        let mut archive = zip::ZipArchive::new(file)?;

        let unzip_path = Path::new(source)
        .parent()
        .unwrap()
        .join("unzipped");

        if unzip_path.exists() {
            self.status_changed("Cleared unzip folder ... ");
            fs::remove_dir_all(unzip_path.clone())?;
        }
    
        for i in 0..archive.len() {
            let mut file = archive.by_index(i)?;
            let subpath = match file.enclosed_name() {
                Some(path) => path.to_owned(),
                None => continue,
            };

            let outpath = unzip_path.join(subpath);

            {
                let comment = file.comment();
                if !comment.is_empty() {
                    self.status_changed(&format!("File {} comment: {}", i, comment));
                }
            }
    
            if (&*file.name()).ends_with('/') {
                self.unzipped_file(i as u32, outpath.to_str().unwrap(), 0);

                fs::create_dir_all(&outpath)?;
            } else {
                self.unzipped_file(i as u32, outpath.to_str().unwrap(), file.size());

                if let Some(p) = outpath.parent() {
                    if !p.exists() {
                        fs::create_dir_all(&p)?;
                    }
                }

                let mut outfile = fs::File::create(&outpath)?;
                io::copy(&mut file, &mut outfile)?;
            }
    
            // Get and Set permissions
            #[cfg(unix)]
            {
                use std::os::unix::fs::PermissionsExt;
    
                if let Some(mode) = file.unix_mode() {
                    fs::set_permissions(&outpath, fs::Permissions::from_mode(mode))?;
                }
            }
        }

        Ok(unzip_path)
    }

    fn transfer_files(&self, unzip_folder: PathBuf) -> Result<(), Box<dyn std::error::Error>> {
        use std::fs;

        let update_path = Path::new(&self.update_path);

        if update_path.exists() {
            fs::remove_dir_all(&self.update_path)?;
        }

        fs::create_dir(update_path)?;

        let mut files: Vec<DirEntry> = Vec::new();
        visit_dirs(&unzip_folder, &mut files)?;

        let mut current_num = 0;
        let total_files = files.len() as u32;

        for entry in files {
            current_num += 1;
            self.transferring(current_num, total_files);

            let path = entry.path();
            let filetype = entry.file_type().unwrap();

            let new_path = path.to_str().unwrap().replace(unzip_folder.to_str().unwrap(), "");
            let new_path = rem_first_and_last(&new_path);
            let new_path = update_path.join(new_path);

            if filetype.is_dir() {
                fs::create_dir(&new_path)?;
            }
            else {
                fs::copy(&path, new_path)?;
            }    
        }

        Ok(())
    }
}

fn rem_first_and_last(value: &str) -> &str {
    let mut chars = value.chars();
    chars.next();
    chars.as_str()
}

fn visit_dirs(dir: &Path, entries: &mut Vec<DirEntry>) -> std::io::Result<()> {
    if dir.is_dir() {
        for entry in std::fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                entries.push(entry);
                visit_dirs(&path, entries)?;
            } else {
                entries.push(entry);
            }
        }
    }
    Ok(())
}

fn get_current_version() -> Result<String, Box<dyn std::error::Error>> {
    let file_path = get_version_path()?;

    return Ok(std::fs::read_to_string(file_path)?);
}

fn get_version_path() -> Result<PathBuf, Box<dyn std::error::Error>> {
    use directories::BaseDirs;
    use std::fs;

    if let Some(base_dirs) = BaseDirs::new() {
        let app_data_path = base_dirs.config_dir().join("jogiupdater");

        if !app_data_path.exists() {
            fs::create_dir(app_data_path.clone())?;
        }

        let version_file_path = app_data_path.join("version.txt");

        return Ok(version_file_path);
    } else {
        panic!("Could not access base dirs!");
    }
}



fn set_current_version(version: &str) -> Result<(), Box<dyn std::error::Error>> {
    let file_path = get_version_path()?;

    let mut file = std::fs::File::create(file_path)?;
    file.write_all(version.as_bytes())?;

    return Ok(());
}
